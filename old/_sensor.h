#define TEMP_MIN 21
#define TEMP_MAX 37
#define HUMID_MIN 0.0
#define HUMID_MAX 1.0


class CTemperatureSensor {
    float Temperature;
    float TLow;
    float THigh;
public:
    CTemperatureSensor();
    CTemperatureSensor(float low, float high);
    float getTemperature();
};

class CHumiditySensor {
    float Humidity;
    float HLow;
    float HHigh;
public:
    CHumiditySensor();
    CHumiditySensor(float low, float high);
    float getHumidity();
};
