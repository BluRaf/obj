#include "sensor.h"
#include "rand.h"

float makeMeasurement(float low, float high)
{
    return getRandomNumber(low, high);
}

CTemperatureSensor::CTemperatureSensor()
{
    TLow = TEMP_MIN;
    THigh = TEMP_MAX;
    Temperature = 0;
}

CTemperatureSensor::CTemperatureSensor(float low, float high)
{
    TLow = low;
    THigh = high;
    Temperature = 0;
}

float CTemperatureSensor::getTemperature()
{
    Temperature = makeMeasurement(TLow, THigh);
    return Temperature;
}

CHumiditySensor::CHumiditySensor()
{
    HLow = HUMID_MIN;
    HHigh = HUMID_MAX;
    Humidity = 0;
}

CHumiditySensor::CHumiditySensor(float low, float high)
{
    HLow = low;
    HHigh = high;
    Humidity = 0;
}

float CHumiditySensor::getHumidity()
{
    Humidity = makeMeasurement(HLow, HHigh);
    return Humidity;
}
