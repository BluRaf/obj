struct SMeasurement {
    float temperature;
    float humidity;
};

class CBase {
    float temp;
    float humid;
    class CTemperatureSensor * temp_ptr;
    class CHumiditySensor * humid_ptr;
public:
    CBase();
    void setTemperatureSensor(CTemperatureSensor * ptr);
    void setHumiditySensor(CHumiditySensor * ptr);
    struct SMeasurement readSensors();
    void displayMeasurements(struct SMeasurement results);
};
