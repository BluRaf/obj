#include <iostream>
#include "sensor.h"
#include "base.h"

CBase::CBase()
{
    temp_ptr = NULL;
    humid_ptr = NULL;
    temp = 0;
    humid = 0;
}

void CBase::setTemperatureSensor(CTemperatureSensor * ptr)
{
   temp_ptr = ptr;
}

void CBase::setHumiditySensor(CHumiditySensor * ptr)
{
    humid_ptr = ptr;
}

struct SMeasurement CBase::readSensors()
{
    struct SMeasurement buf;
    buf.temperature = (*temp_ptr).getTemperature();
    buf.humidity = (*humid_ptr).getHumidity();
    return buf;
}

void CBase::displayMeasurements(struct SMeasurement results)
{
    std::cout << "Temperature: " << results.temperature << std::endl;
    std::cout << "Humidity: " << results.humidity << std::endl;
}
