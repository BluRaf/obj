/*---------------------------------------------------------------------------------------------*/
/* Fragment pliku base.h - pola oznaczone znakami zapytania należy uzupełnić odpowiednim kodem */
/*---------------------------------------------------------------------------------------------*/

#include <vector>
#include "exception.h"
#include <assert.h>	//Plik nagłówkowy niezbędny do poprawnego działania funkcji assert()

class CBase
{
private:
  std::vector<CSensor*> Sensors;

public:
  CBase(unsigned int max = 8)
  {
    Sensors.reserve(max);
  }

  CBase(const CBase& source)
  {
    Sensors = source.Sensors;
  }

  CBase& operator=(const CBase& source)
  {
    Sensors = source.Sensors;
    return * this;
  }

  void setSensor(CSensor* pSensor)	//Metoda dodająca adresy kolejnych czujników
  {
    Sensors.push_back(pSensor);
  }

  void displayMeasurements()	       //Metoda pobierająca i wyświetlająca w konsoli dane z czujników
  {
    for (unsigned int i = 0; i < Sensors.size(); i++) {
      CSensor* pSensor = Sensors[i];
      cout << "Current " << pSensor->getName() << ": " << pSensor->getMeasurement() << ' ' << pSensor->getUnit() << endl;
    }
    cout << endl;
  }
};

