/*-----------------------------------------------------------------------------------------------*/
/* Fragment pliku sensor.h - pola oznaczone znakami zapytania należy uzupełnić odpowiednim kodem */
/*-----------------------------------------------------------------------------------------------*/

#include <string>
using namespace std;

#include "rand.h"

class CSensor
{
private:
	float lowLimit;
	float highLimit;
	float Measurement;

	float makeMeasurement() {return getRandomNumber(lowLimit, highLimit);}

public:
	CSensor(float lowLimit, float highLimit) :
	lowLimit(lowLimit),
	highLimit(highLimit)
	{}

	float getMeasurement()
	{
		Measurement = makeMeasurement();
		return Measurement;
	}

  void checkLimits(float low, float high)
  {
    if (lowLimit < low) lowLimit = low;
    if (highLimit > high) highLimit = high;
  };
  
	virtual string getName() = 0;
	virtual string getUnit() = 0;
};


class CTemperatureSensor : public CSensor
{
public:
  CTemperatureSensor(float lowLimit, float highLimit) :
    CSensor::CSensor(lowLimit, highLimit)
  {
    checkLimits(-40, 85);
  };
  virtual string getName() {return "temperature";};
  virtual string getUnit() {return "oC";};
};


class CHumiditySensor : public CSensor
{
public:
  CHumiditySensor(float lowLimit, float highLimit) :
    CSensor::CSensor(lowLimit, highLimit)
  {
    checkLimits(0.0, 100.0);
  };
  virtual string getName() {return "humidity";};
  virtual string getUnit() {return "%";};
};


class CPressureSensor : public CSensor
{
public:
  CPressureSensor(float lowLimit, float highLimit) :
    CSensor::CSensor(lowLimit, highLimit)
  {
    checkLimits(300, 1100);
  };
  virtual string getName() {return "atmospheric pressure";};
  virtual string getUnit() {return "hPa";};
};


class CWindSensor : public CSensor
{
public:
  CWindSensor(float lowLimit, float highLimit) :
    CSensor::CSensor(lowLimit, highLimit)
  {
    checkLimits(0.0, 1200.0);
  };
  virtual string getName() {return "wind speed";};
  virtual string getUnit() {return "m/s";};
};


class CInsolationSensor : public CSensor
{
public:
  CInsolationSensor(float lowLimit, float highLimit) :
    CSensor::CSensor(lowLimit, highLimit)
  {
    checkLimits(0, 2000);
  };
  virtual string getName() {return "insolation";};
  virtual string getUnit() {return "W/m^2";};
};
